import React, { Component } from 'react'
import { Modal } from 'antd'

const KEY_USER_DATA = 'user_data'

class Profile extends Component {

    state = {
        email: '',
        isShowDialog: false
    }

    componentDidMount() {
        const jsonStr = localStorage.getItem(KEY_USER_DATA)
        this.setState({ email: JSON.parse(jsonStr).email })
    }

    showDialog = () => {
        this.setState({ isShowDialog: true })
    }

    handleCancel = () => {
        this.setState({ isShowDialog: false })
    }

    handleOk = () => {
        localStorage.setItem(KEY_USER_DATA, JSON.stringify(
            {
                isLoggedIn: false
            }
        ))
        this.props.history.push('/')
    }

    render() {
        return (
            <div>
                {/* Profile Page */}
                <h1>Email: {this.state.email}</h1>
                <button style={{
                    padding: '16px 32px 16px 32px',
                    background: 'blue',
                    color: 'white',
                    cursor: 'pointer',
                    fontSize: '18px'
                }}
                    onClick={this.showDialog}
                >Log out</button>
                <Modal
                    title="Basic Modal"
                    visible={this.state.isShowDialog}
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                />
            </div>
        )
    }
}

export default Profile