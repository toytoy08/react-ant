import React, { Component } from 'react';
import { Form, Button, Input, Icon, message } from 'antd';
import { withRouter } from 'react-router-dom';

const KEY_USER_DATA = 'user_data'

class Login extends Component {
  state = {
    email: '',
    password: ''
  };

  navigateTiMainPage = () => {
    const { history } = this.props
    history.push('movies')
  }

  componentDidMount() {
    const jsonStr = localStorage.getItem(KEY_USER_DATA)
    const isLoggedIn = jsonStr && JSON.parse(jsonStr).isLoggedIn
    if (isLoggedIn) {
      this.navigateTiMainPage()
    }
  }

  onEmailChange = event => {
    const email = event.target.value;
    this.setState({ email });
    // console.log(event.target.value)
  };
  onPasswordChange = event => {
    const password = event.target.value;
    this.setState({ password });
  };

  validateEmail(email) {
    var re = /^(([^<>()\]\\.,;:\s@"]+(\.[^<>()\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  validatePassword(password) {
    var re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/;
    return re.test(String(password));
  }

  onSubmitFormLogin = e => {
    e.preventDefault();
    const isValid = this.validateEmail(this.state.email);
    const isValidePassword = this.validatePassword(this.state.password);
    console.log(isValid);
    console.log(isValidePassword);
    if (isValid && isValidePassword) {
      localStorage.setItem(
        KEY_USER_DATA,
        JSON.stringify({
          isLoggedIn: true,
          email: this.state.email
        })
      )
      this.navigateTiMainPage()
    } else {
      message.error('Wrong something', 1);
    }
  };

  render() {
    return (
      <div>
        <h1>Login Page</h1>
        <Form onSubmit={this.onSubmitFormLogin}>
          <Form.Item>
            <Input
              prefix={<Icon type="mail" />}
              placeholder="Email"
              onChange={this.onEmailChange}
            />
          </Form.Item>
          {/* <p>{this.state.email}</p> */}
          <Form.Item>
            <Input
              prefix={<Icon type="lock" />}
              placeholder="Password"
              type="password"
              onChange={this.onPasswordChange}
            />
          </Form.Item>
          <Form.Item>
            <Button htmlType="submit">Submit</Button>
          </Form.Item>
        </Form>
      </div>
    );
  }
}

export default withRouter(Login);
