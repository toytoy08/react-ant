import React, { Component } from 'react';
import { Button } from 'antd';

class Main extends Component {

    facebookLogin() {

    }

    state = {
        loading: false,
        iconLoading: false,
    }

    enterLoading = () => {
        this.setState({ loading: true });
      }
    
      enterIconLoading = () => {
        this.setState({ iconLoading: true });
      }

    render() {
        return (
            <div >
                <Button type="primary"
                loading={this.state.loading}
                onClick={this.enterLoading}
                    style={{
                        width: '30%',
                        margin: '0 auto',
                        position: 'absolute',
                        top: '50%',
                        translateY: '-50%'
                    }}>
                    Login with Facebook
              </Button>
            </div>
        )
    }
}

export default Main